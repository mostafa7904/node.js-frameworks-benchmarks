# Node.js frameworks benchmarks

## This is a project to benchmark some of the most popular node.js frameworks.

frameworks in this project include: 

- Express
- Koa
- Fastify
- ~~node~~ *comming soon*

to run the project you have to install dependecies one by one.

in the root folder type something like this in your command line:

`npm i` or `yarn install`

`cd express-bm`

and then run `npm install` or `yarn install` or `npm i`

and change your directory to the root again : `cd ..`

now run `node index.js --express`

### if you want to run all the frameworks at once you have to make sure you've installed all the dependencies for them

and then just run `node index.js` or `npm run test`

and if you want to run each separately just pass the name to the command like below:

*in the root folder* `node index.js --framework_name`


params you can pass to this are:

- --express | *for express*
- --fastify | *for fastify*
- --koa | *for koa*
- ~~--node | *for node.js*~~ *comming soon*

## testing

in order to test and run the test you can do it from postman
a post man file is in the root folder.

import it in **postman** and run the testing

this test just sends an **array with 1000000** objects in it

the benchmarks times will be added in the next version but for now it's just a simple test

made with ❤ by Mostafa