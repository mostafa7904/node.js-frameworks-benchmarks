const express = require("express");
const app = express();
const array = Array(1000000).fill({
  name: "Mostsfa",
  age: 18,
});
app.get("/get", (req, res) => {
  return res.send(array);
});
app.post("/post", (req, res) => {
  return res.send(array);
});

const startServer = async (port, cb) => {
  await app.listen(port, cb);
};

module.exports = startServer;
