const sysProcces = require("./cli");
const { spawn } = require("child_process");
const main = async () => {
  try {
    const result = await sysProcces();

    if (result === "express") {
      const expressOut = spawn("node", ["express-bm"]);
      expressOut.stdout.on("data", (data) => {
        console.log(data.toString());
      });
    } else if (result === "koa") {
      const koaOut = spawn("node", ["koa-bm"]);
      koaOut.stdout.on("data", (data) => {
        console.log(data.toString());
      });
    } else if (result === "fastify") {
      const fastifyOut = spawn("node", ["fastify-bm"]);
      fastifyOut.stdout.on("data", (data) => {
        console.log(data.toString());
      });
    } else if (result === "all") {
      const expressOut = spawn("node", ["express-bm"]);
      expressOut.stdout.on("data", (data) => {
        console.log(data.toString());
      });
      const koaOut = spawn("node", ["koa-bm"]);
      koaOut.stdout.on("data", (data) => {
        console.log(data.toString());
      });
      const fastifyOut = spawn("node", ["fastify-bm"]);
      fastifyOut.stdout.on("data", (data) => {
        console.log(data.toString());
      });
    }
  } catch (e) {
    console.log("There was an error in running app");
    console.error(e);
  }
};
main();
