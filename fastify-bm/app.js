const fastify = require("fastify");

const app = fastify({ logger: false });
const array = Array(1000000).fill({
  name: "Mostsfa",
  age: 18,
});
app.get("/get", (req, reply) => {
  return reply.send(array);
});
app.post("/post", (req, reply) => {
  return reply.send(array);
});

const startServer = async (port, cb) => {
  await app.listen(port, cb);
};

module.exports = startServer;
