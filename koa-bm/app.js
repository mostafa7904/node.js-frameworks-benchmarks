const Koa = require("koa");
const Router = require("@koa/router");
const app = new Koa();
const router = new Router();
const array = Array(1000000).fill({
  name: "Mostsfa",
  age: 18,
});

router.get("/get", (ctx) => {
  return (ctx.body = array);
});
router.post("/post", (ctx) => {
  return (ctx.body = array);
});

app.use(router.routes());
const startServer = async (port, cb) => {
  await app.listen(port, cb);
};

module.exports = startServer;
