const app = require("./app");

app(3001, () => {
  console.log("Koa app is running on port 3001");
});
