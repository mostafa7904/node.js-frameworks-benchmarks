const os = require("os");
const c = require("chalk");

// console theme
const boldOrange = c.hex("#F27E63").bold;
const boldRed = c.hex("#D95284").bold;
const boldPurple = c.hex("#977EF2").bold;
const boldBlue = c.hex("#977EF2").bold;
const blue = c.blue;
const bgBlue = c.bgHex("#0597F2");
const log = console.log;

// gigabytes for dividing
const GigaByte = 1e9;

// console arguments
const args = process.argv;

async function sysProcces() {
  try {
    // empty logs for better view in console
    log();
    log();

    log(bgBlue("All apps are starting"));
    log(bgBlue("Use the postman file in the root folder to test the project"));

    // system info
    const cpus = os.cpus();
    const model = cpus[0].model;
    const cpuCount = cpus.length;
    // const usage = process.cpuUsage();
    const version = os.version || parseInt(os.release()).toFixed(2);
    const platform = os.platform();
    // get memory in gigabytes
    const freeMem = os.freemem() / GigaByte;
    const totalMem = os.totalmem() / GigaByte;
    const memPrecent = ((freeMem / totalMem) * 100).toFixed(2);
    log();
    log(
      blue("App is running in"),
      boldPurple(platform),
      blue("version"),
      boldPurple(typeof version === "function" ? version() : version)
    );
    log(boldBlue("CPU cors count :"), boldOrange(cpuCount, "cors"));
    log(boldBlue("CPU model :"), boldOrange(model));
    log(boldBlue("Total ram :"), boldOrange(totalMem, "GigaBytes"));
    log(boldBlue("Free ram :"), boldOrange(freeMem, "GigaBytes"));
    log();
    log(boldOrange(memPrecent, "% of ram is free"));
    log();
    if (args.includes("--express")) {
      log(boldRed("--express flag detected"));
      log(boldRed("running express only"));
      log();
      log();
      return "express";
    } else if (args.includes("--koa")) {
      log(boldRed("--koa flag detected"));
      log(boldRed("running koa only"));
      log();
      log();
      return "koa";
    } else if (args.includes("--fastify")) {
      log(boldRed("--fastify flag detected"));
      log(boldRed("running fastify only"));
      log();
      log();
      return "fastify";
    } else {
      log(boldRed("no flag detected"));
      log(boldRed("running all apps"));
      log();
      log();
      return "all";
    }
  } catch (error) {
    console.log("There was a problem in index.js file in sysProcces");
    console.error(error);
  }
}

module.exports = sysProcces;
